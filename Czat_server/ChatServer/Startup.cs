﻿/*Create by Marcin Gołuński*/
using System;
using System.IO;
using System.Linq;
using ChatServerMG.Enum;
using ChatServerMG.Komendy;
using ChatServerMG.Komendy.DoKlienta;
using ChatServerMG.Komendy.DoSerwera;
using ChatServerMG.Users;
using ChatServerMG.WebSocet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ChatServerMG
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddWebSocketManager();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
     /*                   WhoConnect.Instance.AddAdmin("xx", "xx","ss");
            var k = WhoConnect.Instance.Admins.FirstOrDefault(x => x.UserId == "xx");
            WhoConnect.Instance.RemoveAdmin("xx");
            WhoConnect.Instance.AddAdmin("xx", "xx", "ss");
            WhoConnect.Instance.RemoveAdmin("xx");

            WhoConnect.Instance.AddUnknow("xx", "ss");
            var k1 = WhoConnect.Instance.Unknow.Where(x => x.UserId == "xx");
            WhoConnect.Instance.RemoveUnknow("xx");
            WhoConnect.Instance.AddUnknow("xx", "ss");
            WhoConnect.Instance.RemoveUnknow("xx");


            WhoConnect.Instance.AddUser("xx", "xx",2,3, "ss", "pl");
            var k2 = WhoConnect.Instance.Users.Where(x => x.UserId == "xx");
            WhoConnect.Instance.RemoveUser("xx");
            WhoConnect.Instance.AddUser("xx", "xx", 2, 3, "ss", "pl");
            WhoConnect.Instance.RemoveUser("xx");
*/

    /*        var  hist = new History(333);
            hist.Add("ola", 55, "1");
            hist.Add("Igor (admin)", 55, "2");
            hist.Add("ola", 55, "3");
            hist.Add("Igor (admin)", 55, "4");
            hist.Add("ola", 55, "5");
            hist.Add("ola", 55, "6");
            hist.Add("ola", 55, "7");
            hist.Add("ola", 55, "8");
            hist.Add("ola", 55, "9");
            hist.Add("ola", 55, "10");
            hist.Add("Igor (admin)", 55, "11");
            hist.Add("ola", 55, "12");
            hist.Add("ola", 55, "13");
            hist.Add("ola", 55, "14");
            hist.Add("Igor (admin)", 55, "15");
            hist.Add("ola", 55, "16");
            hist.Add("ola", 55, "17");
            hist.Add("ola", 55, "18");
            hist.Add("ola", 55, "19");
            hist.Add("ola", 55, "20");
            hist.Add("Igor (admin)", 55, "21", "Ktos");
            hist.Add("ola", 55, "22");
            hist.Add("Igor (admin)", 55, "23", "Igor");
            hist.Add("ola", 55, "24");

            var k = hist.Get(0, 5, out var err);

            var cos = new CommandPatternToClient(CommandNameToClient.MessagesList, new MessagesList("kk", k));
            var test = cos.ToString();*/

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            var wscOption = new WebSocketOptions()
            {
                KeepAliveInterval = TimeSpan.FromSeconds(60),
                ReceiveBufferSize = 1024 * 1024,
                
            };
            app.UseWebSockets(wscOption);
            app.MapWebSocketManager("/chat", serviceProvider.GetService<ChatRoomHandler>());
            app.UseStaticFiles();
            app.UseMvc();
        }

    }
}
