﻿/*Create by Marcin Gołuński*/
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using ChatServerMG.Komendy;
using ChatServerMG.Komendy.DoKlienta;
using Microsoft.Data.Sqlite;

namespace ChatServerMG
{
    public class History
    {
        private string filePath;
        private SqliteConnection sqlbase;
        private static string NameTable = "History";
        private static string ParameterId = "Id";
        private static string Username = "Username";
        private static string Message = "Message";
        private static string Time = "Time";
        private static string SupervisorName = "SupervisorName";
        public History(int userID, out Error error)
        {
            error = null;
            var path = Path.Combine(Environment.CurrentDirectory, "history");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            filePath = Path.Combine(path, $"{userID}.sqlite3");

            if (!File.Exists(filePath))
            {
                error = new Error("1002","Brak pliku");
            }
            else
            {
                sqlbase = new SqliteConnection("Filename=" + filePath);

            }
        }
        public History(int userID)
        {
            var path = Path.Combine(Environment.CurrentDirectory, "history");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            filePath = Path.Combine(path, $"{userID}.sqlite3");

            if (!File.Exists(filePath))
            {
                sqlbase = new SqliteConnection("Filename=" + filePath);
                sqlbase.Open();

                using (var command = sqlbase.CreateCommand())
                {
                    command.CommandText = $@"CREATE TABLE IF NOT EXISTS {NameTable}
                    (
                        [{ParameterId}] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, [{Username}] VARCHAR(64) NOT NULL, [{Message}] VARCHAR(512) NOT NULL, [{Time}] BIGINT NOT NULL, [{SupervisorName}] VARCHAR(512) NOT NULL
                        )  
                    ";
                    command.ExecuteNonQuery();
                }
                sqlbase.Close();
            }
            else

            {
                sqlbase = new SqliteConnection("Filename=" + filePath);

            }
        }

        public void Add(string characterName, long time, string message, string supervisorName = "user")
        {
            sqlbase.Open();
            using (var command = new SqliteCommand{ Connection = sqlbase })
            { 
                    command.CommandText =$@"INSERT INTO {NameTable} ({Username}, {Message}, {Time},
                               {SupervisorName}) VALUES (@characterName, @message, @time, @supervisorName)";
                    command.Parameters.AddWithValue("@characterName", characterName);
                    command.Parameters.AddWithValue("@message", message);
                    command.Parameters.AddWithValue("@time", time);
                    command.Parameters.AddWithValue("@supervisorName", supervisorName);
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();

            }
            /*    using (var command = sqlbase.CreateCommand())
                {
                    command.CommandText = $@"INSERT INTO `{NameTable}` (`{Username}`, `{Message}`, `{Time}`, `{SupervisorName}`) VALUES ('{characterName}', '{message}', '{time}', '{supervisorName}')";


                    // Create table if not exist  
                    command.ExecuteNonQuery();
                }*/
            sqlbase.Close();
        }

        public List<MessageItem> Get(int messageId, int count, out Error error)
        {
            error = null;
            string errorM = string.Empty;
            sqlbase.Open();
            List<MessageItem> result = new List<MessageItem>();
            using (var command = sqlbase.CreateCommand())
            {
                command.CommandText =
                    $@"SELECT * FROM {NameTable} ORDER BY {ParameterId} DESC LIMIT {messageId},{count}";

                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (long.TryParse(reader.GetString(3), out long time))
                        {
                            result.Add(new MessageItem(reader.GetString(1), reader.GetString(2), time, reader.GetString(4)));
                        }
                        else
                        {
                            errorM += $"Cannot parse: {reader.GetString(3)} to long"; 
                            error = new Error("1001", errorM);
                            sqlbase.Close();
                            return result;
                        }
                    }
                }
                
            }
            sqlbase.Close();
            return result;
        }
    }

}
