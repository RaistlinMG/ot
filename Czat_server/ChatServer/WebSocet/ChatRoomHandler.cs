﻿/*Create by Marcin Gołuński*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ChatServer.Komendy.ToClient;
using ChatServerMG.Enum;
using ChatServerMG.Komendy;
using ChatServerMG.Komendy.AnswerFromIGOR;
using ChatServerMG.Komendy.DoKlienta;
using ChatServerMG.Komendy.DoSerwera;
using ChatServerMG.Users;
using Newtonsoft.Json;

namespace ChatServerMG.WebSocet
{
    public class ChatRoomHandler : WebSocketHandler
    {
        public ChatRoomHandler(WebSocketConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
        }

        public override async Task OnConnected(WebSocket socket, string ipAddress)
        {
            /*var test = "[0, 1, 2, 3]";
            var k = JsonConvert.DeserializeObject<List<int>>(test);
            var cos = "{\"Command\":\"SetBlockTerminals\",\"Data\":{\"BlockedTerminalIds\":[1],\"BlockedMessage\":\"dasdas\"}}";
            var message =
                JsonConvert.DeserializeObject<CommandSetBlockTerminals>(cos);
            var data = (SetBlockTerminals)message.Data;
            WhoConnect.Instance.SetBlocktTermianlId(new List<int> {4, 3, 5}, "ala");*/
            try
            {
                await base.OnConnected(socket, ipAddress);
                var socketId = WebSocketConnectionManager.GetId(socket);
                WhoConnect.Instance.AddUnknow(socketId, ipAddress);
        //        await SendMessageToAllAsync($"{socketId} is now connected");
            //    await SendMessageAsync(socketId,
            //        new CommandPatternToClient(CommandNameToClient.Connected,
            //            null));


  /*              WhoConnect.Instance.AddUser($"55","test",2,3,"ss","pl");
                WhoConnect.Instance.AddAdmin($"{socketId}", "test1", "ss");
                WhoConnect.Instance.AddUserToAdmin(socketId,"55");
                //test

                var test = $"{{\"Command\":\"CloseSession\",\"Data\":{{\"SessionId\":\"55\"}}}}";
                var message =
                    JsonConvert.DeserializeObject<CommandCloseSession>(test);
                var data = (CloseSession)message.Data;

                if (WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId) &&
                    WhoConnect.Instance.Admins.Any(x => x.SessionIds.Contains(data.SessionId)))
                {
           //         await SendCloseMessageAsync(data.SessionId, new CommandPatternToClient(CommandNameToClient.SessionClosed, new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds())).ToString());
                    WhoConnect.Instance.RemoveUser(data.SessionId);
                    WhoConnect.Instance.RemoveUserFromAdmin(data.SessionId);
                    await SendMessageToAllAdminAsync(new CommandPatternToClient(
                        CommandNameToClient.SessionClosed,
                        new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                            data.Message)));
                    await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                        new UserList()));
                }else if (WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId))
                {
                    await SendCloseMessageAsync(data.SessionId, new CommandPatternToClient(CommandNameToClient.SessionClosed, new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds())).ToString());
                    WhoConnect.Instance.RemoveUser(data.SessionId);
                    await SendMessageToAllAdminAsync(new CommandPatternToClient(
                        CommandNameToClient.SessionClosed,
                        new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                            data.Message)));
                    await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                        new UserList()));
                }
                else
                {
                    Console.WriteLine($"Users found: {WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId)}, Admins found: {WhoConnect.Instance.Admins.Any(x => x.SessionIds.Contains(data.SessionId))}");
                }
                */



            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception in On Connected {ex}");
                await SendCloseMessageAsync(WebSocketConnectionManager.GetId(socket), new CommandPatternToClient(CommandNameToClient.Error,
                    null, new Error("1", ex.Message)).ToString());
            }
        }
        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var cos = Encoding.UTF8.GetString(buffer, 0, result.Count);
            var socketId = WebSocketConnectionManager.GetId(socket);
            CommandNameToServer commnad;
            try
            {
                commnad = findCommand(Encoding.UTF8.GetString(buffer, 0, result.Count));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception In find Command exception:{ex}");
                await SendMessageAsync(socketId,
                    new CommandPatternToClient(CommandNameToClient.Error,
                        null, new Error("1", ex.Message)));
                return;
            }

            try
            {
                switch (commnad)
                {
                    case CommandNameToServer.AcceptSession:
                    {
                        try
                        {
                            try
                            {
                                var message =
                                    JsonConvert.DeserializeObject<CommandAcceptSession>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                                var data = (AcceptSession) message.Data;

                                if (WhoConnect.Instance.Admins.Any(x => x.SessionId == socketId))
                                {
                                    WhoConnect.Instance.AddUserToAdmin(socketId, data.SessionId);
                                    WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == data.SessionId).Connect();
                                    await SendMessageAsync(data.SessionId,
                                        new CommandPatternToClient(CommandNameToClient.SessionOpened,
                                            new SessionOpened(data.SessionId)));
                                    await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                                        new UserList()));
                                }
                                else
                                {
                                    await SendMessageAsync(socketId,
                                        new CommandPatternToClient(CommandNameToClient.SessionOpened,
                                            null, new Error("2", $"Nie znaleziono admina o sessionID: {socketId}")));
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Exception In AcceptSession exception:{ex}");
                                if (ex is NullReferenceException)
                                {
                                    await SendMessageAsync(socketId,
                                        new CommandPatternToClient(CommandNameToClient.SessionOpened,
                                            null, new Error("2", "Prawdopodobnie Nie znaleziono admina albo użytkownika")));
                                }
                                else
                                {
                                    await SendMessageAsync(socketId,
                                        new CommandPatternToClient(CommandNameToClient.SessionOpened,
                                            null, new Error("1", ex.Message)));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In AcceptSession in catch exception:{ex}");
                            await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.Error,
                                    null, new Error("1", ex.Message)));
                            }

                        break;
                    }
                    case CommandNameToServer.CloseSession:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandCloseSession>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (CloseSession) message.Data;

                            if (WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId) &&
                                WhoConnect.Instance.Admins.Any(x => x.SessionIds.Contains(data.SessionId)))
                            {
                                await SendCloseMessageAsync(data.SessionId,
                                    new CommandPatternToClient(CommandNameToClient.SessionClosed, new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(), data.Message))
                                        .ToString());
                                WhoConnect.Instance.RemoveUser(data.SessionId);
                                WhoConnect.Instance.RemoveUserFromAdmin(data.SessionId);
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.SessionClosed,
                                    new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(), data.Message)));
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList, new UserList()));
                            }
                            else if (WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId))
                            {
                                await SendCloseMessageAsync(data.SessionId,
                                    new CommandPatternToClient(CommandNameToClient.SessionClosed, new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(), data.Message))
                                        .ToString());
                                WhoConnect.Instance.RemoveUser(data.SessionId);
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(
                                    CommandNameToClient.SessionClosed,
                                    new SessionClosed(data.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                        data.Message)));
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                                    new UserList()));
                            }
                            else
                            {
                                Console.WriteLine(
                                    $"Users found: {WhoConnect.Instance.Users.Any(x => x.SessionId == data.SessionId)}, Admins found: {WhoConnect.Instance.Admins.Any(x => x.SessionIds.Contains(data.SessionId))}");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In CloseSession exception:{ex}");
                            await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionClosed,
                                null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.GetLastMessages:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandGetLasMessages>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (GetLastMessages) message.Data;


                            var historii = new History(data.UserId, out var error);
                            if (error != null)
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.MessagesList,
                                    null, error));
                                break;
                            }

                            var messages = historii.Get(data.Offset, data.Count, out error);

                            if (error == null)
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.MessagesList,
                                    new MessagesList(data.UserId, messages)));
                            }
                            else
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.MessagesList,
                                    null, error));
                            }

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In GetLastMessages exception:{ex}");
                                await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.MessagesList,
                                    null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.GetSendMessage:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandGetSendMessage>(
                                    Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (GetSendMessage) message.Data;
                            var time = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                            var historii = new History(WhoConnect.Instance.Users
                                .FirstOrDefault(x => x.SessionId == data.SessionId).UserID);
                            if (WhoConnect.Instance.Users.Any(x => x.SessionId == socketId))
                            {
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.Message,
                                    new SendMessageUser(data.SessionId, WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == data.SessionId)
                                        .CharacterName, data.Message, time)));
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.Message,
                                    new SendMessageUser(data.SessionId, WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == data.SessionId)
                                        .CharacterName, data.Message, time)));

                                historii.Add(WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == data.SessionId)
                                    .CharacterName, time, data.Message);

                            }
                            else
                            {
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.Message,
                                    new SendMessageAdmin(data.SessionId,
                                        $"{WhoConnect.Instance.SupervisorDisplayName}({WhoConnect.Instance.Admins.FirstOrDefault(x => x.SessionId == socketId).CharacterName})",
                                        data.Message, time)));
                                await SendMessageAsync(data.SessionId, new CommandPatternToClient(CommandNameToClient.Message,
                                    new SendMessageAdmin(data.SessionId, $"{WhoConnect.Instance.SupervisorDisplayName}", data.Message, time)));

                                historii.Add(
                                    $"{WhoConnect.Instance.SupervisorDisplayName}({WhoConnect.Instance.Admins.FirstOrDefault(x => x.SessionId == socketId).CharacterName})",
                                    time, data.Message, WhoConnect.Instance.SupervisorDisplayName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In GetSendMessage exception:{ex}");
                                if (ex is NullReferenceException)
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.Message,
                                    null, new Error("2", "Prawdopodobnie Nie znaleziono admina albo użytkownika")));
                            }
                            else
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.Message,
                                    null, new Error("1", ex.Message)));
                            }
                        }

                        break;
                    }
                    case CommandNameToServer.GetServerConfig:
                    {
                        try
                        {
                            if (WhoConnect.Instance.Admins.Any(x => x.SessionId == socketId))
                            {
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.ServerConfig,
                                    new ServerConfig(WhoConnect.Instance.SupervisorDisplayName,
                                        WhoConnect.Instance.AwayMessagePl, WhoConnect.Instance.AwayMessageEng)));
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In GetServerConfig exception:{ex}");
                                await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.ServerConfig,
                                    null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.GetSessionDetails:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandGetSessionDetails>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (GetSessionDetails) message.Data;

                            var user = WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == data.SessionId);
                            var admin = WhoConnect.Instance.Admins.FirstOrDefault(
                                x => x.SessionIds.Contains(data.SessionId));
                            await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionDetails,
                                new SessionDetails(data.SessionId, user.Langugage, user.CharacterName, user.ConnectedTime,
                                    user.StartTime, admin.CharacterName, admin.Ip, (int) user.TerminalId, user.Ip)));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In GetSessionDetails exception:{ex}");
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionDetails,
                                null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.RequestSession:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandRequestSession>(
                                    Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (RequestSession) message.Data;
                            HttpResponseMessage respond = null;
                            using (var client = new HttpClient())
                            {
                                string uri;
                                using (StreamReader r = new StreamReader("ipIgor.json"))
                                {
                                    uri = r.ReadToEnd();
                                }
                                    client.BaseAddress = new Uri(uri);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "terminal/chat/authorize");

                                request.Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                                {
                                    new KeyValuePair<string, string>("requestToken", data.RequestToken)
                                });
                                await client.SendAsync(request)
                                    .ContinueWith(responseTask => { respond = responseTask.Result; });
                            }

                            var messageFromIgor = respond.Content.ReadAsStringAsync().Result;
                            int respondCode = respond.StatusCode.GetHashCode();
                            if (respondCode == 200)
                            {
                                var answer =
                                    JsonConvert.DeserializeObject<CommandAuthorizeClientAnswer>(messageFromIgor);

                                if (answer.Error != null)
                                {
                                    await SendMessageAsync(socketId,
                                        new CommandPatternToClient(CommandNameToClient.SessionVerify,
                                            null, answer.Error));
                                    break;
                                }

                                var who = (AuthorizeClientAnswer) answer.Data;
                                var sessionWho = WhoConnect.Instance.Unknow.FirstOrDefault(x => x.SessionId == socketId);
                                if (who.IsAdmin)
                                {
                                    WhoConnect.Instance.AddAdmin(socketId, who.ClientName, sessionWho.Ip);
                                    await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionVerify,
                                        new SessionVerify(true)));
                                    await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.UserList, new UserList()));
                                }
                                else
                                {
                                    if (WhoConnect.Instance.BlockedTerminalIds.Contains((int) who.TerminalId))
                                    {
                                        await SendCloseMessageAsync(socketId, new CommandPatternToClient(
                                            CommandNameToClient.SessionClosed,
                                            new SessionClosed(socketId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                                WhoConnect.Instance.BlockedMessage)).ToString());
                                    }
                                    else
                                    {
                                        WhoConnect.Instance.AddUser(socketId, who.ClientName, who.ClientId, (int) who.TerminalId, sessionWho.Ip, data.Language);
                                        await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList, new UserList()));
                                        await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionVerify,
                                            new SessionVerify(true)));
                                    }
                                }

                                WhoConnect.Instance.RemoveUnknow(socketId);
                            }
                            else
                            {
                                await SendCloseMessageAsync(socketId,
                                    new CommandPatternToClient(CommandNameToClient.SessionVerify,
                                        null, new Error($"{respondCode}", messageFromIgor)).ToString());
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In RequestSession exception:{ex}");
                                await SendCloseMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.SessionVerify,
                                    null, new Error("1", ex.Message)).ToString());
                        }

                        break;
                    }
                    case CommandNameToServer.SetServerConfig:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandSetServerConfig>(Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (SetServerConfig) message.Data;
                            WhoConnect.Instance.SetSupervisorDisplayName(data.SupervisorDisplayName);
                            WhoConnect.Instance.SetAwayMessage(data.AwayMessagePl, data.AwayMessageEng);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In SetServerConfig exception:{ex}");
                                await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.SessionDetails, //??
                                    null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.SetTopic:
                    {
                        try
                        {
                            if (WhoConnect.Instance.Users.Any(x => x.SessionId == socketId))
                            {
                                var message =
                                    JsonConvert.DeserializeObject<CommandSetTopic>(
                                        Encoding.UTF8.GetString(buffer, 0, result.Count));
                                var data = (SetTopic) message.Data;
                                WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == socketId)
                                    .SetTopic(data.TopicMessage);
                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                                    new UserList()));
                            }

                            await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.AddTopic, new AddTopic()));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In SetTopic exception:{ex}");
                                await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.AddTopic, null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.SetBlockTerminals:
                    {
                        try
                        {
                            var message =
                                JsonConvert.DeserializeObject<CommandSetBlockTerminals>(
                                    Encoding.UTF8.GetString(buffer, 0, result.Count));
                            var data = (SetBlockTerminals) message.Data;

                            WhoConnect.Instance.SetBlocktTermianlId(data.BlockedTerminalIds, data.BlockedMessage);

                            foreach (var terminalId in WhoConnect.Instance.BlockedTerminalIds)
                            {
                                if (WhoConnect.Instance.Users.Any(x => x.TerminalId == terminalId))
                                {
                                    var user = WhoConnect.Instance.Users.FirstOrDefault(x => x.TerminalId == terminalId);
                                    await SendCloseMessageAsync(user.SessionId, new CommandPatternToClient(
                                        CommandNameToClient.SessionClosed,
                                        new SessionClosed(user.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                            WhoConnect.Instance.BlockedMessage)).ToString());
                                    await SendMessageToAllAdminAsync(new CommandPatternToClient(
                                        CommandNameToClient.SessionClosed,
                                        new SessionClosed(user.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                            WhoConnect.Instance.BlockedMessage)));
                                    WhoConnect.Instance.RemoveUser(user.SessionId);
                                    WhoConnect.Instance.RemoveUserFromAdmin(user.SessionId);
                                }
                                else if (WhoConnect.Instance.Unknow.Any(x => x.TerminalId == terminalId))
                                {
                                    var unknown = WhoConnect.Instance.Unknow.FirstOrDefault(x => x.TerminalId == terminalId);
                                    await SendCloseMessageAsync(unknown.SessionId, new CommandPatternToClient(
                                        CommandNameToClient.SessionClosed,
                                        new SessionClosed(unknown.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                            WhoConnect.Instance.BlockedMessage)).ToString());
                                    await SendMessageToAllAdminAsync(new CommandPatternToClient(
                                        CommandNameToClient.SessionClosed,
                                        new SessionClosed(unknown.SessionId, DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                                            WhoConnect.Instance.BlockedMessage)));
                                    WhoConnect.Instance.RemoveUnknow(unknown.SessionId);
                                }

                                await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList,
                                    new UserList()));
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In SetBlockTerminals exception:{ex}");
                            await SendMessageAsync(socketId,
                                new CommandPatternToClient(CommandNameToClient.SessionClosed,
                                    null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    case CommandNameToServer.GetBlockedTerminals:
                    {
                        try
                        {
                            await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SendBlockedTerminals,
                                new SendBlockedTerminals(WhoConnect.Instance.BlockedTerminalIds, WhoConnect.Instance.BlockedMessage)));
                        }

                        catch (Exception ex)
                        {
                            Console.WriteLine($"Exception In GetBlockedTerminals exception:{ex}");
                                await SendMessageAsync(socketId, new CommandPatternToClient(CommandNameToClient.SessionDetails,
                                null, new Error("1", ex.Message)));
                        }

                        break;
                    }
                    default:
                    {
                        Console.WriteLine($"Exception In defaoult switch command");
                        await SendMessageAsync(socketId,
                            new CommandPatternToClient(CommandNameToClient.Error,
                                null, new Error("1", $"Dont use state: {commnad}")));
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception In switch casse exception:{ex}");
                await SendMessageAsync(socketId,
                    new CommandPatternToClient(CommandNameToClient.Error,
                        null, new Error("1", ex.Message)));
            }
        }

        private CommandNameToServer findCommand(string buffer)
        {
            if (buffer.Contains(CommandNameToServer.AcceptSession.ToString()))
            {
                return CommandNameToServer.AcceptSession;
            }

            if (buffer.Contains(CommandNameToServer.CloseSession.ToString()))
            {
                return CommandNameToServer.CloseSession;
            }

            if (buffer.Contains(CommandNameToServer.GetLastMessages.ToString()))
            {
                return CommandNameToServer.GetLastMessages;
            }

            if (buffer.Contains(CommandNameToServer.GetServerConfig.ToString()))
            {
                return CommandNameToServer.GetServerConfig;
            }

            if (buffer.Contains(CommandNameToServer.GetSessionDetails.ToString()))
            {
                return CommandNameToServer.GetSessionDetails;
            }

            if (buffer.Contains(CommandNameToServer.RequestSession.ToString()))
            {
                return CommandNameToServer.RequestSession;
            }

            if (buffer.Contains(CommandNameToServer.GetSendMessage.ToString()))
            {
                return CommandNameToServer.GetSendMessage;
            }

            if (buffer.Contains(CommandNameToServer.SetServerConfig.ToString()))
            {
                return CommandNameToServer.SetServerConfig;
            }

            if (buffer.Contains(CommandNameToServer.SetTopic.ToString()))
            {
                return CommandNameToServer.SetTopic;
            }

            if (buffer.Contains(CommandNameToServer.SetBlockTerminals.ToString()))
            {
                return CommandNameToServer.SetBlockTerminals;
            }

            if (buffer.Contains(CommandNameToServer.GetBlockedTerminals.ToString()))
            {
                return CommandNameToServer.GetBlockedTerminals;
            }

            throw new Exception($"Not found command in {buffer}");
        }
    }
}