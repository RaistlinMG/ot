﻿/*Create by Marcin Gołuński*/
using System;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ChatServerMG.Enum;
using ChatServerMG.Komendy;
using ChatServerMG.Komendy.DoKlienta;
using ChatServerMG.Users;

namespace ChatServerMG.WebSocet
{
    public abstract class WebSocketHandler
    {
        protected WebSocketConnectionManager WebSocketConnectionManager { get; set; }

        public WebSocketHandler(WebSocketConnectionManager webSocketConnectionManager)
        {
            WebSocketConnectionManager = webSocketConnectionManager;
        }

        public virtual async Task OnConnected(WebSocket socket, string ipAddress)
        {
            WebSocketConnectionManager.AddSocket(socket);            
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            try
            {
                try
                {
                    if (WhoConnect.Instance.Users.Any(x => x.SessionId == WebSocketConnectionManager.GetId(socket)))
                    {
                        WhoConnect.Instance.RemoveUser(WebSocketConnectionManager.GetId(socket));
                        await SendMessageToAllAdminAsync(new CommandPatternToClient(CommandNameToClient.UserList, new UserList()));
                    }
                    else if (WhoConnect.Instance.Admins.Any(x => x.SessionId == WebSocketConnectionManager.GetId(socket)))
                    {
                        var admin = WhoConnect.Instance.Admins.FirstOrDefault(x => x.SessionId == WebSocketConnectionManager.GetId(socket));
                        WhoConnect.Instance.Admins.FirstOrDefault(x => x == admin).Disconnect();
                        Thread.Sleep(15000);
                        if (WhoConnect.Instance.Admins.Any(x => x.CharacterName == admin.CharacterName && x.IsConnect))
                        {
                            WhoConnect.Instance.RemoveAdmin(admin.SessionId);
                        }
                        else
                        {
                            foreach (var sessionId in admin.SessionIds)
                            {
                                WhoConnect.Instance.Users.FirstOrDefault(x => x.SessionId == sessionId).Disconnect();
                            }

                            WhoConnect.Instance.RemoveAdmin(admin.SessionId);
                        }
                    }
                    else if (WhoConnect.Instance.Unknow.Any(x => x.SessionId == WebSocketConnectionManager.GetId(socket)))
                    {
                        WhoConnect.Instance.RemoveUnknow(WebSocketConnectionManager.GetId(socket));
                    }

                    var df = WebSocketConnectionManager.GetSocketById(WebSocketConnectionManager.GetId(socket));
                    df.Dispose();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Exception In OnDisconnected exception:{ex}");
                    await SendMessageAsync(WebSocketConnectionManager.GetId(socket), new CommandPatternToClient(CommandNameToClient.SessionClosed,
                        null, new Error("1", ex.Message)));
                }
                finally
                {
                    await WebSocketConnectionManager.RemoveSocket(WebSocketConnectionManager.GetId(socket));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception In Finally in OnDisconnected exception:{ex}");
            }
        }

        public async Task SendMessageAsync(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
                return;
            
            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.UTF8.GetBytes(message)),
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
        }
        public async Task SendCloseMessageAsync(WebSocket socket, string message)
        {
            if (socket.State != WebSocketState.Open)
                return;
            await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.UTF8.GetBytes(message)),
                messageType: WebSocketMessageType.Text,
                endOfMessage: true,
                cancellationToken: CancellationToken.None);
            
          //  await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, message, CancellationToken.None);
            await OnDisconnected(socket);
        }
        public async Task SendCloseMessageAsync(string socketId, string message)
        {
            await SendCloseMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message);
        }
        public async Task SendMessageAsync(string socketId, string message)
        {
            await SendMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message);
        }
        public async Task SendMessageAsync(string socketId, CommandPatternToClient message)
        {
            await SendMessageAsync(WebSocketConnectionManager.GetSocketById(socketId), message.ToString());
        }
        public async Task SendMessageToAllAdminAsync(CommandPatternToClient message, string skipSessionID = "-100")
        {
            foreach (var admin in WhoConnect.Instance.Admins)
            {
                if (admin.IsConnect && admin.SessionId != skipSessionID)
                {
                    await SendMessageAsync(admin.SessionId, message.ToString());
                }
            }
        }

        public async Task SendMessageToAllAdminAsync(string message, string skipSessionID = "-100")
        {
            foreach (var admin in WhoConnect.Instance.Admins)
            {
                if (admin.IsConnect && admin.SessionId != skipSessionID)
                {
                    await SendMessageAsync(admin.SessionId, message);
                }
            }
        }
        public async Task SendMessageToAllAsync(string message)
        {
            foreach (var pair in WebSocketConnectionManager.GetAll())
            {
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessageAsync(pair.Value, message);
            }
        }

        public abstract Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer);
    }
}
