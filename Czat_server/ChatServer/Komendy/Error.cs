﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy
{
    public class Error
    {
        private string code;
        private string message;

        public Error(string code, string message)
        {
            this.code = code;
            this.message = message;
        }

        public string Code => code;
        public string Message  => message;
    }
}
