﻿/*Create by Marcin Gołuński*/
using Newtonsoft.Json;

namespace ChatServerMG.Komendy
{
    public abstract class IData
    {
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);            
        }
    }
}
