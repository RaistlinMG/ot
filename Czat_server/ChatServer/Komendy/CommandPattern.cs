﻿/*Create by Marcin Gołuński*/
using Newtonsoft.Json;

namespace ChatServerMG.Komendy
{
    public abstract class CommandPattern
    {
        private IData data;
        private Error error;

        public CommandPattern(IData data, Error error = null)
        {
            this.data = data;
            this.error = error;
        }

        public virtual IData Data => data; 
        public Error Error => error;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
