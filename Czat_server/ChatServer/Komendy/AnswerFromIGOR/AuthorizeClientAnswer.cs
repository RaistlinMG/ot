﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class AuthorizeClientAnswer : IData
    {
        private bool isAdmin;
        private int clientId;
        private string clientName;
        private int? terminalId;

        public AuthorizeClientAnswer(bool isAdmin, int clientId, string clientName, int? terminalId = null)
        {
            this.isAdmin = isAdmin;
            this.clientId = clientId;
            this.clientName = clientName;
            this.terminalId = terminalId;
        }

        private AuthorizeClientAnswer()
        {
        }

        public bool IsAdmin => isAdmin;
        public int ClientId => clientId;
        public string ClientName => clientName;
        public int? TerminalId => terminalId;
    }
}
