﻿/*Create by Marcin Gołuński*/
using System;

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SessionOpened : IData
    {
        private string sessionId;
        private long time;
        private SessionOpened() { }
        public SessionOpened(string sessionId)
        {
            this.sessionId = sessionId;
            this.time = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        public string SessionId => sessionId;
        public long Time  => time;
    }
}
