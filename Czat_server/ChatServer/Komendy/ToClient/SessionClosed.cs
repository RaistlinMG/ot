﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SessionClosed : IData
    {
        private string sessionId;
        private string message;
        private long time;
        private SessionClosed() { }
        public SessionClosed(string sessionId, long time, string message = null)
        {
            this.sessionId = sessionId;
            this.message = message;
            this.time = time;
        }

        public string SessionId => sessionId;
        public string Message => message;
        public long Time => time;
    }
}
