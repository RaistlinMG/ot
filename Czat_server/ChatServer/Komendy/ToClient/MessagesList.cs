﻿/*Create by Marcin Gołuński*/
using System.Collections.Generic;

namespace ChatServerMG.Komendy.DoKlienta
{
    public class MessagesList : IData
    {
        private int _userId;
        private List<MessageItem> messages;
        private MessagesList() { }

        public MessagesList(int userId, List<MessageItem> messages)
        {
            this._userId = userId;
            this.messages = messages;
        }

        public int UserId => _userId;
        public List<MessageItem> Messages => messages;
    }

    public class MessageItem
    {
        private string administratorId;
        private string message;
        private long time;
        private string supervisorName;

        public MessageItem(string administratorId, string message, long time, string supervisorName)
        {
            this.administratorId = administratorId;
            this.message = message;
            this.time = time;
            this.supervisorName = supervisorName;
        }
        public string AdministratorId => administratorId;
        public string Message => message;
        public long Time => time;
        public string SupervisorName => supervisorName;
    }
}
