﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SessionVerify : IData
    {
        private bool accept;

        public SessionVerify(bool accept)
        {
            this.accept = accept;
        }

        public bool Accept => accept;
    }
}
