﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SessionDetails : IData
    {
        private string sessionId;
        private string locale;
        private string characterId;
        private long connectedTime;
        private long startedTime;
        private string supervisorId;
        private string supervisorIp;
        private int terminalId;
        private string terminalIp;
        private SessionDetails() { }
        public SessionDetails(string sessionId, string locale, string characterId, long connectedTime, long startedTime, string supervisorId, string supervisorIp, int terminalId, string terminalIp)
        {
            this.sessionId = sessionId;
            this.locale = locale;
            this.characterId = characterId;
            this.connectedTime = connectedTime;
            this.startedTime = startedTime;
            this.supervisorId = supervisorId;
            this.supervisorIp = supervisorIp;
            this.terminalId = terminalId;
            this.terminalIp = terminalIp;
        }

        public string SessionId => sessionId;
        public string Locale => locale;
        public string CharacterId => characterId;
        public long ConnectedTime => connectedTime;
        public long StartedTime => startedTime;
        public string SupervisorId => supervisorId;
        public string SupervisorIp => supervisorIp;
        public int TerminalId => terminalId;
        public string TerminalIp => terminalIp;
    }
}
