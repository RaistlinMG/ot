﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class ServerConfig : IData
    {
        private string supervisorDisplayName;
        private string defaultAwayMessagePl;
        private string defaultAwayMessageEng;
        private ServerConfig() { }
        public ServerConfig(string supervisorDisplayName, string defaultAwayMessagePl, string defaultAwayMessageEng)
        {
            this.supervisorDisplayName = supervisorDisplayName;
            this.defaultAwayMessagePl = defaultAwayMessagePl;
            this.defaultAwayMessageEng = defaultAwayMessageEng;
        }

        public string SupervisorDisplayName => supervisorDisplayName;
        public string DefaultAwayMessagePl => defaultAwayMessagePl;
        public string DefaultAwayMessageEng => defaultAwayMessageEng;
    }
}
