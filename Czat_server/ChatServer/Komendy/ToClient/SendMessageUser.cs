﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SendMessageUser : IData
    {
        private string sessionId;
        private string characterId;
        private string message;
        private long time;
        private SendMessageUser() { }
        public SendMessageUser(string sessionId, string characterId, string message, long time)
        {
            this.sessionId = sessionId;
            this.characterId = characterId;
            this.message = message;
            this.time = time;
        }

        public string SessionId => sessionId;
        public string CharacterId => characterId;
        public string Message => message;
        public long Time => time;
    }
}
