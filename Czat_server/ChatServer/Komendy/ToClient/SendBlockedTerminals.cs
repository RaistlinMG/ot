﻿/*Create by Marcin Gołuński*/
using System.Collections.Generic;

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SendBlockedTerminals : IData
    {
        private List<int> blockedTermianls = new List<int>();
        private string blockedMessage;

        public SendBlockedTerminals(List<int> blockedTermianls, string blockedMessage)
        {
            this.blockedTermianls = blockedTermianls;
            this.blockedMessage = blockedMessage;
        }

        public List<int> BlockedTerminals => blockedTermianls;
        public string BlockedMessage => blockedMessage;
    }
}
