﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoKlienta
{
    public class SendMessageAdmin : IData
    {
        private string sessionId;
        private string administratorId;
        private string message;
        private long time;
        private SendMessageAdmin() { }
        public SendMessageAdmin(string sessionId, string administratorId, string message, long time)
        {
            this.sessionId = sessionId;
            this.administratorId = administratorId;
            this.message = message;
            this.time = time;
        }

        public string SessionId => sessionId;
        public string AdministratorId => administratorId;
        public string Message => message;
        public long Time => time;
    }
}
