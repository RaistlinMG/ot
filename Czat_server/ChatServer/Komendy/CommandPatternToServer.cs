﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ChatServerMG.Komendy
{
    public class CommandPatternToServer : CommandPattern
    {
        private CommandNameToServer command;
        public CommandPatternToServer(CommandNameToServer command, IData data, Error error = null) : base(data, error)
        {
            this.command = command;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public CommandNameToServer Command => command;
    }
}
