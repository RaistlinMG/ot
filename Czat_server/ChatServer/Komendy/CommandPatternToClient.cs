﻿/*Create by Marcin Gołuński*/

using System.Collections.Generic;
using System.Text.RegularExpressions;
using ChatServerMG.Enum;
using ChatServerMG.Komendy.DoKlienta;
using ChatServerMG.Users;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ChatServerMG.Komendy
{
    public class CommandPatternToClient : CommandPattern
        {
            private CommandNameToClient command;
            public CommandPatternToClient(CommandNameToClient command, IData data, Error error = null) : base(data, error)
            {
                this.command = command;
            }
        [JsonConverter(typeof(StringEnumConverter))]
        public CommandNameToClient Command => command;
            public override string ToString()
            {
                var result = base.ToString();
                if (command == CommandNameToClient.MessagesList)
                {
                    var data = (MessagesList)Data;
                    var regs = Regex.Matches(result, "\"AdministratorId\":\".*?\"");
                    int i = 0;
                    foreach (var reg in regs)
                    {
                        var character = reg.ToString();
                        
                        if (data.Messages[i].SupervisorName == "user")
                        {
                            var newCharacter = character.Replace("AdministratorId", "CharacterId");
                            result = result.Replace(character, newCharacter);
                            break;
                        }
                        i++;
                    }
                }
                return result;
            }
    }
}
