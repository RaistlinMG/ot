﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandGetSendMessage : CommandPatternToServer
    {
        public CommandGetSendMessage(CommandNameToServer command, GetSendMessage data, Error error = null) : base(command, data, error)
        {
        }
    }
}
