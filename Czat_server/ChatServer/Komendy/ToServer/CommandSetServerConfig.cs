﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandSetServerConfig : CommandPatternToServer
    {
        public CommandSetServerConfig(CommandNameToServer command, SetServerConfig data, Error error = null) : base(command, data, error)
        {
        }
    }
}
