﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandGetSessionDetails : CommandPatternToServer
    {
        public CommandGetSessionDetails(CommandNameToServer command, GetSessionDetails data, Error error = null) : base(command, data, error)
        {
        }
    }
}
