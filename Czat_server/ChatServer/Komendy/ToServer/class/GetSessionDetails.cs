﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class GetSessionDetails : IData
    {
        private string sessionId;
        private GetSessionDetails() { }
        public GetSessionDetails(string sessionId)
        {
            this.sessionId = sessionId;
        }

        public string SessionId => sessionId;
    }
}
