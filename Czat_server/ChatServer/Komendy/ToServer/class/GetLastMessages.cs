﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class GetLastMessages : IData
    {
        private int userID;
        private int offset;
        private int count;


        private GetLastMessages() { }
        public GetLastMessages(int userId, int offset = 0, int count = 10)
        {
            this.offset = offset;
            this.count = count;
            this.userID = userId;
        }

        public int UserId => userID;
        public int Offset => offset;
        public int Count => count;
    }
}
