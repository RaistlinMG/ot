﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class RequestSession : IData
    {
        private string requestToken;
        private string language;
        private RequestSession() { }
        public RequestSession(string requestToken, string language)
        {
            this.requestToken = requestToken;
            this.language = language;
        }

        public string RequestToken => requestToken;
        public string Language => language;
    }
}
