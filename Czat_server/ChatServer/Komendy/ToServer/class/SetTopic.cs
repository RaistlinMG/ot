﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class SetTopic : IData
    {
        private string topicMessage;

        public SetTopic(string topicMessage)
        {
            this.topicMessage = topicMessage;
        }
        private SetTopic() { }

        public string TopicMessage => topicMessage;
    }
}
