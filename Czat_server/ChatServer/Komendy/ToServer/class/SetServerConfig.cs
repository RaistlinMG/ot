﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class SetServerConfig : IData
    {
        private string supervisorDisplayName;
        private string awayMessagePl;
        private string awayMessageEng;
        private SetServerConfig() { }
        public SetServerConfig(string supervisorDisplayName, string awayMessagePl = null, string awayMessageEng = null)
        {
            this.supervisorDisplayName = supervisorDisplayName;
            this.awayMessagePl = awayMessagePl;
            this.awayMessageEng = awayMessageEng;
        }

        public string SupervisorDisplayName => supervisorDisplayName;
        public string AwayMessagePl => awayMessagePl;
        public string AwayMessageEng => awayMessageEng;
    }
}
