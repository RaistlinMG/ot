﻿/*Create by Marcin Gołuński*/
using System.Collections.Generic;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class SetBlockTerminals : IData
    {
        private List<int> blockedTerminalIds;
        private string blockedMessage;

        public SetBlockTerminals(List<int> blockedTerminalIds, string blockedMessage = null)
        {
            this.blockedTerminalIds = blockedTerminalIds;
            this.blockedMessage = blockedMessage;
        }

        private SetBlockTerminals() { }

        public List<int> BlockedTerminalIds => blockedTerminalIds;
        public string BlockedMessage => blockedMessage;
    }
}
