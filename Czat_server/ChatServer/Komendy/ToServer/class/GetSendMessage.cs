﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class GetSendMessage : IData
    {
        private string sessionId;
        private string message;
        private GetSendMessage() { }
        public GetSendMessage(string sessionId, string message)
        {
            this.sessionId = sessionId;
            this.message = message;
        }

    public string SessionId => sessionId;
        public string Message => message; 
    }
}
