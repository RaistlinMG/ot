﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class AcceptSession : IData
    {
        private string sessionId;

        public AcceptSession(string sessionId)
        {
            this.sessionId = sessionId;
        }
        private AcceptSession() { }
        public string SessionId => sessionId;
    }
}
