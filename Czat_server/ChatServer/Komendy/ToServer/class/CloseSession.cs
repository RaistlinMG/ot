﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CloseSession : IData
    {
        private string sessionId;
        private string message;

        private CloseSession() { }
        public CloseSession(string sessionId, string message = null)
        {
            this.sessionId = sessionId;
            this.message = message;
        }

        public string SessionId => sessionId;
        public string Message => message;
    }
}
