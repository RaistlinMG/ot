﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandGetLasMessages : CommandPatternToServer
    {
        public CommandGetLasMessages(CommandNameToServer command, GetLastMessages data, Error error = null) : base(command, data, error)
        {
        }
    }
}
