﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandGetBlockedTerminal : CommandPatternToServer
    {
        public CommandGetBlockedTerminal(CommandNameToServer command, GetBlockedTerminal data, Error error = null) : base(command, data, error)
        {
        }
    }
}
