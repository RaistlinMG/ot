﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandSetTopic : CommandPatternToServer
    {
        public CommandSetTopic(CommandNameToServer command, SetTopic data, Error error = null) : base(command, data, error)
        {
        }
    }
}
