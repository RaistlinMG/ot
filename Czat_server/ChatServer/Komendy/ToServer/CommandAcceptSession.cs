﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandAcceptSession : CommandPatternToServer
    {
        public CommandAcceptSession(CommandNameToServer command, AcceptSession data, Error error = null) : base(command, data, error)
        {
        }
    }
}
