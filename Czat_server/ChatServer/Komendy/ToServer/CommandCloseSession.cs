﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandCloseSession : CommandPatternToServer
    {
        public CommandCloseSession(CommandNameToServer command, CloseSession data, Error error = null) : base(command, data, error)
        {
        }
    }
}
