﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandRequestSession : CommandPatternToServer
    {
        public CommandRequestSession(CommandNameToServer command, RequestSession data, Error error = null) : base(command, data, error)
        {
        }
    }
}
