﻿/*Create by Marcin Gołuński*/

using ChatServerMG.Enum;

namespace ChatServerMG.Komendy.DoSerwera
{
    public class CommandSetBlockTerminals : CommandPatternToServer
    {
        public CommandSetBlockTerminals(CommandNameToServer command, SetBlockTerminals data, Error error = null) : base(command, data, error)
        {
        }
    }
}
