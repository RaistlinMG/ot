﻿/*Create by Marcin Gołuński*/
using System;
using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ChatServerMG
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Chat Server create by Marcin Gołuński");
            try
            {
                BuildWebHost(args).Run();
            }
            catch(Exception ex)
            {              
                Console.WriteLine(ex);
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("hosting.json", optional: true)
                    .Build()
                )
                .UseStartup<Startup>()
                .Build();
    }
}
