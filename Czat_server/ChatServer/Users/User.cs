﻿/*Create by Marcin Gołuński*/
using System;

namespace ChatServerMG.Users
{
    public class User : UserPattern
    {
        private long connectedTime;
        private long startTime;
        private bool isConnect = false;
        private string language;
        private string topic = "";
        private int userId;

        public User(string sessionId, string characterName, int userId, int terminalId, string ip, string language) : base(sessionId, characterName, terminalId, ip)
        {
            this.connectedTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            this.language = language;
            this.userId = userId;
        }

        public long ConnectedTime => connectedTime;
        public long StartTime => startTime;
        public string Langugage => language;
        public string Topic => topic;
        public bool IsConnect => isConnect;
        public int UserID => userId;

        public void Connect()
        {
            this.startTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            isConnect = true;
        }

        public void Disconnect()
        {
            this.connectedTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            isConnect = false;
        }

        public void SetTopic(string topicMessage)
        {
            topic = topicMessage;
        }
    }
}