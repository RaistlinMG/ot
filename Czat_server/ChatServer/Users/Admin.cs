﻿/*Create by Marcin Gołuński*/
using System.Collections.Generic;

namespace ChatServerMG.Users
{
    public class Admin : UserPattern
    {
        private List<string> sessionIds = new List<string>();
        private bool isConnect = true;

        public Admin(string sessionId, string characterName, string ip) : base(sessionId, characterName, null, ip)
        {
        }

        public Admin(string sessionId, string ip, Admin admin) : base(sessionId, admin.CharacterName, null, ip)
        {
            foreach (var userSessionId in admin.SessionIds)
            {
                sessionIds.Add(userSessionId);
            }
        }

        public List<string> SessionIds => sessionIds;
        public bool IsConnect => isConnect;

        public void AddUser(string sessionId)
        {
            this.sessionIds.Add(sessionId);
        }
        public void RemoveUser(string sessionId)
        {
            this.sessionIds.Remove(sessionId);
        }
        public void Disconnect()
        {
            isConnect = false;
        }
    }
}