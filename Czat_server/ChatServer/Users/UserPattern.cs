﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Users
{
    public abstract class UserPattern
    {
        protected string sessionId;
        private string characterName;
        private int? terminalId;
        private string ip;

        public UserPattern(string sessionId, string characterName, int? terminalId, string ip)
        {
            this.sessionId = sessionId;
            this.terminalId = terminalId;
            this.ip = ip;
            this.characterName = characterName;
        }
        public int? TerminalId => terminalId;
        public string CharacterName => characterName;
        public string Ip => ip;
        public string SessionId => sessionId;
    }
}
