﻿/*Create by Marcin Gołuński*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatServerMG.Users
{
    public class WhoConnect
    {
        private List<Admin> admins = new List<Admin>();
        private List<User> users = new List<User>();
        private List<Unknow> unknow = new List<Unknow>();
        private string supervisorDisplayName = "Igor";
        private string awayMessagePl = "Przeprowadzam kalkulację. Proszę czekać.";
        private string awayMessageEng = "I'm making a calculation. Please wait.";
        private List<int> blockedTerminalIds = new List<int>();
        private string blockedMessage;
        private lockVarables adminLock = new lockVarables();
        private lockVarables userLock = new lockVarables();
        private lockVarables unknowLock = new lockVarables();
        private lockVarables blokedLock = new lockVarables();
        private lockVarables nameAndAwayLock = new lockVarables();
        private static readonly WhoConnect instance = new WhoConnect();
        private WhoConnect()
        {
        }

        public static WhoConnect Instance => instance;


        public List<Admin> Admins
        {
            get
            {
                adminLock.TryOpenRead();
                try
                {
                    return admins;
                }
                finally
                {
                    adminLock.CloseRead();
                }
            }
            private set { }
        }

        internal void AddUserToAdmin(string sessionIdAdmin, string sessionIdUser)
        {
            adminLock.TryOpenWrite();
            try
            {
                admins.FirstOrDefault(x => x.SessionId == sessionIdAdmin).AddUser(sessionIdUser);
            }
            finally
            { adminLock.CloseWrite();

            }
        }

        public List<User> Users
        {
            get
            {
                userLock.TryOpenRead();
                try
                {
                    return users;
                }
                finally
                {
                    userLock.CloseRead();
                }
            }
        }

        public List<Unknow> Unknow
        {
            get
            {
                unknowLock.TryOpenRead();
                try
                {
                    return unknow;
                }
                finally
                {

                    unknowLock.CloseRead();
                }
            }
        }

        public string SupervisorDisplayName
        {
            get
            {
                nameAndAwayLock.TryOpenRead();
                try
                {
                    return supervisorDisplayName;
                }
                finally
                {
                    nameAndAwayLock.CloseRead();
                }

            }
        }

        public string AwayMessagePl
        {
            get
            {
                nameAndAwayLock.TryOpenRead();
                try
                {
                    return awayMessagePl;
                }
                finally
                {
                    nameAndAwayLock.CloseRead();
                }

            }
        }

        public string AwayMessageEng
        {
            get
            {
                nameAndAwayLock.TryOpenRead();
                try
                {
                    return awayMessageEng;
                }
                finally
                {
                    nameAndAwayLock.CloseRead();
                }

            }
        }

        public List<int> BlockedTerminalIds
        {
            get
            {
                blokedLock.TryOpenRead();
                try
                {
                    return blockedTerminalIds;
                }
                finally
                {
                    blokedLock.CloseRead();
                }

            }
        }

        public string BlockedMessage
        {
            get
            {
                blokedLock.TryOpenRead();
                try
                {
                    return blockedMessage;
                }
                finally
                {
                    blokedLock.CloseRead();
                }

            }
        }

        public void AddAdmin(string sessionId, string characterName, string ip)
        {
            adminLock.TryOpenWrite();
            try
            {
                if (admins.Any(x => x.CharacterName == characterName))
                {
                    admins.Add(new Admin(sessionId, ip, admins.FirstOrDefault(x => x.CharacterName == characterName)));
                }
                else
                {
                    admins.Add(new Admin(sessionId, characterName, ip));
                }
            }
            finally
            {
                adminLock.CloseWrite();
            }
        }

        public void SetSupervisorDisplayName(string name)
        {
            nameAndAwayLock.TryOpenWrite();
            try
            {
                supervisorDisplayName = name;
            }
            finally
            {
                nameAndAwayLock.CloseWrite();
            }
        }

        public void AddBlockTerminalId(int id)
        {
            blokedLock.TryOpenWrite();
            try
            {
                blockedTerminalIds.Add(id);
            }
            finally
            {
                blokedLock.CloseWrite();
            }
        }

        public void DeleteBlockTerminalID(int id)
        {
            blokedLock.TryOpenWrite();
            try
            {
                blockedTerminalIds.Remove(id);
            }
            finally
            {
                blokedLock.CloseWrite();
            }
        }

        public void SetBlocktTermianlId(List<int> list = null, string message = null)
        {
            blokedLock.TryOpenWrite();
            try
            {
                blockedTerminalIds.RemoveAll(x => true);
                if (list != null && list.Any())
                {
                    foreach (var element in list)
                    {
                        blockedTerminalIds.Add(element);
                    }
                }

                blockedMessage = message;
            }
            finally
            {
                blokedLock.CloseWrite();
            }
        }

        public void SetAwayMessage(string pl, string eng)
        {
            nameAndAwayLock.TryOpenWrite();
            if (!string.IsNullOrWhiteSpace(pl))
            {
                awayMessagePl = pl;
            }

            if (!string.IsNullOrWhiteSpace(eng))
            {
                awayMessageEng = eng;
            }
            nameAndAwayLock.CloseWrite();
        }

        public void AddUnknow(string sessionId, string ip)
        {
            unknowLock.TryOpenWrite();
            try
            {
                unknow.Add(new Unknow(sessionId, ip));
            }
            finally
            {
                unknowLock.CloseWrite();
            }
        }

        public void AddUser(string sessionId, string characterName, int characterID, int terminalId, string ip, string language)
        {
            userLock.TryOpenWrite();
            try
            {
                users.Add(new User(sessionId, characterName, characterID, terminalId, ip, language));
            }
            finally
            {
                userLock.CloseWrite();
            }
        }

        public void RemoveUser(string sessionId)
        {
            userLock.TryOpenWrite();
            try
            {
                users.Remove(users.FirstOrDefault(x => x.SessionId == sessionId));
            }
            finally
            {
                userLock.CloseWrite();
            }
        }
        public void RemoveUserFromAdmin(string sessionId)
        {
            adminLock.TryOpenWrite();
            try
            {
                admins.FirstOrDefault(x => x.SessionIds.Contains(sessionId)).RemoveUser(sessionId);
            }
            finally
            {
                adminLock.CloseWrite();
            }
        }
        public void RemoveAdmin(string sessionId)
        {
            adminLock.TryOpenWrite();
            try
            {
                admins.Remove(admins.FirstOrDefault(x => x.SessionId == sessionId));
            }
            finally
            {
                adminLock.CloseWrite();
            }
        }

        public void RemoveUnknow(string sessionId)
        {
            unknowLock.TryOpenWrite();
            try
            {
                unknow.Remove(unknow.FirstOrDefault(x => x.SessionId == sessionId));
            }
            finally
            {
                unknowLock.CloseWrite();
            }
        }

        private class lockVarables
        {
            private int timeoutInSec;
            private bool canRead = true;
            private bool canWrite = true;


            public lockVarables(int timeoutInSec = 2)
            {
                this.timeoutInSec = timeoutInSec;
            }

            public void TryOpenRead()
            {
                var time = DateTime.Now;
                while (DateTime.Now < time.AddSeconds(timeoutInSec))
                {
                    if (!canRead)
                    {
                        continue;
                    }
                    canWrite = false;
                    return;
                }

                throw new Exception("Cannot get variable -> timeout in read");
            }

            public void CloseRead()
            {
                canWrite = true;
            }

            public void TryOpenWrite()
            {
                canRead = false;
                var time = DateTime.Now;
                while (DateTime.Now < time.AddSeconds(timeoutInSec))
                {
                    if (canWrite)
                    {
                        canWrite = false;
                        canRead = false;
                        return;
                    }
                }
                throw new Exception("Cannot get variable -> timeout in write");
            }

            public void CloseWrite()
            {
                canRead = true;
                canWrite = true;
            }
        }
    }
}
