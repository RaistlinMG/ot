﻿/*Create by Marcin Gołuński*/
using System;

namespace ChatServerMG.Users
{
    public class Unknow : UserPattern
    {
        private long connectedTime;

        public Unknow(string sessionId, string ip) : base(sessionId, "", -1, ip)
        {
            this.connectedTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        }

        public long ConnectedTime => connectedTime;

    }
}