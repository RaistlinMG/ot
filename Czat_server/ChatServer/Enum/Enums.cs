﻿/*Create by Marcin Gołuński*/

namespace ChatServerMG.Enum
{
    public enum CommandNameToServer
    {
        RequestSession,
        GetSendMessage,
        GetLastMessages,
        GetSessionDetails,
        AcceptSession,
        CloseSession,
        GetServerConfig,
        SetServerConfig,
        SetTopic,
        SetBlockTerminals,
        GetBlockedTerminals,
        AuthorizeClient
    }

    public enum CommandNameToClient
    {
        SessionOpened,
        SessionClosed,
        Message,
        MessagesList,
        SessionDetails,
        UserList,
        ServerConfig,
        SendBlockedTerminals,
        SessionVerify,
        AddTopic,
        Error,
        Connected
    }
}
