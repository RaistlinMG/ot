﻿using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Android.Webkit;
using Java.Interop;
using Android.Content;
using Android.Nfc;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace OTAdmin
{
    [Activity(Label = "OTAdmin", MainLauncher = true, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class MainActivity : Activity
    {
        private NfcAdapter nfcAdapter;
        public string uid;
        private bool waitForInfo;
        private IgorMobileInterface jsobject;
        public override void OnBackPressed()
        {
            if (webView.CanGoBack())
            {
                webView.GoBack();
            }
            else
            {
                base.OnBackPressed();
            }
        }
        WebView webView;
        public string URL
        {
            get => UserSettings.URL;
            set
            {
                UserSettings.URL = value;
                NotifyPropertyChanged("URL");
            }
        }

        //string webURL = "https://igor.othub.ovh/admin";
        //string webURL = "http://192.168.0.105/tjtest/number-guessing-game-start.html";
        private Button _writeTagButton;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            global::Android.Webkit.WebView.SetWebContentsDebuggingEnabled(true);
            if (UserSettings.URL == string.Empty)
            {
                UserSettings.URL = @"http://192.168.1.10/admin";
            }
            var alert = new AlertDialog.Builder(this).Create();
           /* if (!CheckIP.CheckPing(UserSettings.URL, this))
            {
                alert.SetMessage($"Cannt ping: {UserSettings.URL} change addres?");
                alert.SetTitle("Ping");
                alert.SetButton("Yes", delegate
                {
                    UserSettings.URL = @"https://igor.othub.ovh/admin";
                });
                alert.SetButton("No", delegate {
                });
                alert.Show();
                alert.Wait();
            }
            */
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.activity_main);
            webView = FindViewById<WebView>(Resource.Id.webView1);
            webView.Settings.JavaScriptEnabled = true;
            webView.Settings.UseWideViewPort = true;
            webView.Settings.LoadWithOverviewMode = true;
            webView.Settings.BuiltInZoomControls = true;
            webView.SetInitialScale(-1);
            webView.Settings.DatabaseEnabled = true;
            webView.Settings.DomStorageEnabled = true;

            // webView.Settings.LoadWithOverviewMode = true;
            //webView.Settings.UseWideViewPort = true;
            if (savedInstanceState != null)
                ((WebView)FindViewById(Resource.Id.webView1)).RestoreState(savedInstanceState);

            webView.SetWebViewClient(new HybridWebViewClient());
            // Render the view from the type generated from RazorView.cshtml 
            jsobject = new IgorMobileInterface(this);
            webView.AddJavascriptInterface(jsobject, "IgorMobileInterface");
            if (savedInstanceState == null)
            {

                webView.LoadUrl(UserSettings.URL);

            }
            nfcAdapter = NfcAdapter.GetDefaultAdapter(this);
            alert = new AlertDialog.Builder(this).Create();
            alert.SetMessage("To read tag enable NFC.");
            alert.SetTitle("Remember");
            alert.SetButton("OK", delegate {
            });
            alert.Show();
        }
        protected override void OnNewIntent(Intent intent)
        {
            if (waitForInfo)
            {
                waitForInfo = false;
                var tag = intent.GetParcelableExtra(NfcAdapter.ExtraTag) as Tag;

                if (tag == null)
                {
                    return;
                }
                
                if (Build.VERSION.SdkInt >= Build.VERSION_CODES.Kitkat)
                {
                    webView.EvaluateJavascript(string.Format("javascript:externalTagCallback('{0}');", ByteArrayToString(tag.GetId())), null);
                }
                else
                {
                    webView.LoadUrl(string.Format("javascript:externalTagCallback('{0}');", ByteArrayToString(tag.GetId())));
                }
            }
            
        }
        private static string ByteArrayToString(byte[] ba)
        {
            var hex = string.Empty;
            for (int i = 0; i < 6; i++)
            {
                if(i < ba.Length)
                {
                    hex += ba[i].ToString("X2") + "-";
                }
                else if(i != 6)
                {
                    hex += "00-";
                }
            }
            if (ba.Length == 7)
            {
                hex += ba[6].ToString("X2");
            }
            else
            {
                hex += "00";
            }
            return hex;
        }
        protected override void OnPause()
        {
            base.OnPause();
            nfcAdapter.DisableForegroundDispatch(this);
        }

        protected override void OnResume()
        {
            base.OnResume();
            EnableWriteMode();
        }
        private void EnableWriteMode()
        {

            waitForInfo = true;

            // Create an intent filter for when an NFC tag is discovered.  When
            // the NFC tag is discovered, Android will u
            var tagDetected = new IntentFilter(NfcAdapter.ActionTagDiscovered);
            var filters = new[] { tagDetected };

            // When an NFC tag is detected, Android will use the PendingIntent to come back to this activity.
            // The OnNewIntent method will invoked by Android.
            var intent = new Intent(this, GetType()).AddFlags(ActivityFlags.SingleTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, 0);

            if (nfcAdapter == null)
            {
                var alert = new AlertDialog.Builder(this).Create();
                alert.SetMessage("NFC is not supported on this device.");
                alert.SetTitle("NFC Unavailable");
                alert.SetButton("OK", delegate {
                });
                alert.Show();
            }
            else
                nfcAdapter.EnableForegroundDispatch(this, pendingIntent, filters, null);
        }
        public class HybridWebViewClient : WebViewClient
        {
            public override bool ShouldOverrideUrlLoading(WebView view, string url)
            {
                view.LoadUrl(url);
                return true;
            }
            public override void OnPageStarted(WebView view, string url, Android.Graphics.Bitmap favicon)
            {
                base.OnPageStarted(view, url, favicon);
            }
            public override void OnPageFinished(WebView view, string url)
            {
                base.OnPageFinished(view, url);
            }
            public override void OnReceivedError(WebView view, [GeneratedEnum] ClientError errorCode, string description, string failingUrl)
            {
                base.OnReceivedError(view, errorCode, description, failingUrl);
            }

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class IgorMobileInterface : Java.Lang.Object
    {
        private Context context;
        private string a = "aaa";
        public IgorMobileInterface(Context context)
        {
            this.context = context;
        }
        [Export]
        [JavascriptInterface]
        public string ShowToast()
        {            
            return a;
            
        }

        public void ChangeValue(int i)
        {
            a += i;

        }
    }
}