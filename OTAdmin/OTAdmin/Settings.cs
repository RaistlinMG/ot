﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace OTAdmin
{
    public static class UserSettings
    {
        static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }
    
        public static string URL
        {
            get => AppSettings.GetValueOrDefault(nameof(URL), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(URL), value);
        }
    
        public static void ClearAllData()
        {
            AppSettings.Clear();
        }
    
    }
}